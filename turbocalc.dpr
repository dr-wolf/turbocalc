program turbocalc;

{$APPTYPE CONSOLE}
{$R *.res}

uses
  SysUtils;

type
  TProc = procedure;

var
  fExpression: string;
  fResult: Real;
  fCS: array of Byte;
  fDS: array of Real;
  fProc: TProc;

function Brackets(Ex: string; First: Integer): Integer;
var
  i, BrQ: Integer;
begin
  Result := 0;
  case Ex[First] of
    '(':
      begin
        i := First + 1;
        BrQ := 0;
        while (i <= length(Ex)) do
        begin
          if (BrQ = 0) and (Ex[i] = ')') then
          begin
            Result := i;
            Exit;
          end;
          if Ex[i] = '(' then
            Inc(BrQ)
          else if Ex[i] = ')' then
            Dec(BrQ);
          i := i + 1;
        end;
      end;
    ')':
      begin
        i := First - 1;
        BrQ := 0;
        while (i > 0) do
        begin
          if (BrQ = 0) and (Ex[i] = '(') then
          begin
            Result := i;
            Exit;
          end;
          if Ex[i] = '(' then
            Inc(BrQ)
          else if Ex[i] = ')' then
            Dec(BrQ);
          i := i - 1;
        end;
      end;
  end;
end;

function PreCalc(Ex: string): Real;
const
  OP: array [0 .. 3] of Char = ('+', '-', '/', '*');
var
  Sc, i, j: Integer;
  s, s1: string;
  A, B: Real;
begin
  s := '';

  for i := 1 to length(Ex) do
    if Ex[i] <> ' ' then
      s := s + Ex[i];

  while Brackets(s, length(s)) = 1 do
    s := copy(s, 2, length(s) - 2);

  if s = '' then
  begin
    Result := 0;
    Exit;
  end;

  Val(s, Result, i);

  if i = 0 then
  begin
    SetLength(fDS, High(fDS) + 2);
    fDS[High(fDS)] := Result;
    SetLength(fCS, High(fCS) + 4);
    fCS[High(fCS)] := High(fDS) * 8;
    fCS[High(fCS) - 1] := $40;
    fCS[High(fCS) - 2] := $DD;
    Exit;
  end;

  i := -1;
  j := 0;
  while j <= 1 do
  begin
    i := length(s);
    Sc := 0;
    while i > 0 do
    begin
      if s[i] = ')' then
        Inc(Sc);
      if s[i] = '(' then
        Dec(Sc);
      if Sc <> 0 then
      begin
        Dec(i);
        Continue;
      end;
      if (s[i] = OP[j * 2]) then
      begin
        j := j * 2 + 10;
        break;
      end;
      if (s[i] = OP[j * 2 + 1]) then
      begin
        j := j * 2 + 11;
        break;
      end;
      Dec(i);
    end;
    Inc(j);
  end;

  case j of
    11:
      begin
        PreCalc(copy(s, 1, i - 1));
        PreCalc(copy(s, i + 1, length(s) - i));
        SetLength(fCS, High(fCS) + 3);
        fCS[High(fCS)] := $C1;
        fCS[High(fCS) - 1] := $DE;
      end;
    12:
      begin
        PreCalc(copy(s, 1, i - 1));
        PreCalc(copy(s, i + 1, length(s) - i));
        SetLength(fCS, High(fCS) + 3);
        fCS[High(fCS)] := $E9;
        fCS[High(fCS) - 1] := $DE;
      end;
    13:
      begin
        try
          PreCalc(copy(s, 1, i - 1));
          PreCalc(copy(s, i + 1, length(s) - i));
          SetLength(fCS, High(fCS) + 3);
          fCS[High(fCS)] := $F9;
          fCS[High(fCS) - 1] := $DE;
        except
          PreCalc(copy(s, 1, i - 1));
          PreCalc(copy(s, i + 1, length(s) - i));
          Exit;
        end;
      end;
    14:
      begin
        PreCalc(copy(s, 1, i - 1));
        PreCalc(copy(s, i + 1, length(s) - i));
        SetLength(fCS, High(fCS) + 3);
        fCS[High(fCS)] := $C9;
        fCS[High(fCS) - 1] := $DE;
      end;
  end;
end;

function Prepare(Ex: string): Real;
begin
  SetLength(fDS, 1);

  SetLength(fCS, 6);
  fCS[0] := $8B;
  fCS[1] := $05;
  fCS[2] := (Integer(@fDS) and $000000FF) shr 0;
  fCS[3] := (Integer(@fDS) and $0000FF00) shr 8;
  fCS[4] := (Integer(@fDS) and $00FF0000) shr 16;
  fCS[5] := (Integer(@fDS) and $FF000000) shr 24;

  PreCalc(Ex);

  SetLength(fCS, High(fCS) + 7);
  fCS[High(fCS) - 5] := $DD;
  fCS[High(fCS) - 4] := $1D;
  fCS[High(fCS) - 3] := (Integer(@fResult) and $000000FF) shr 0;
  fCS[High(fCS) - 2] := (Integer(@fResult) and $0000FF00) shr 8;
  fCS[High(fCS) - 1] := (Integer(@fResult) and $00FF0000) shr 16;
  fCS[High(fCS) - 0] := (Integer(@fResult) and $FF000000) shr 24;

  SetLength(fCS, High(fCS) + 2);

  fCS[High(fCS)] := $C3;

  fProc := Pointer(fCS);
end;

begin
  DecimalSeparator := '.';

  Writeln('TurboCalc v1.0');
  Write('> ');
  Readln(fExpression);

  Prepare(fExpression);
  if (@fProc <> nil) then
    fProc;

  Writeln(fResult:10:4);
end.
